package terekhov.digital.breakthrough.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigitalBreakthroughBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(DigitalBreakthroughBackendApplication.class, args);
    }

}
