package terekhov.digital.breakthrough.backend.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import terekhov.digital.breakthrough.backend.domain.Pack;
import terekhov.digital.breakthrough.backend.dto.*;
import terekhov.digital.breakthrough.backend.repository.AttractionRepository;
import terekhov.digital.breakthrough.backend.repository.PackRepository;
import terekhov.digital.breakthrough.backend.repository.PlaceRepository;
import terekhov.digital.breakthrough.backend.repository.ShowplaceRepository;
import terekhov.digital.breakthrough.backend.service.PackService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PackServiceImpl implements PackService {
    private static final Logger log = LoggerFactory.getLogger(PackService.class);
    private final PackRepository packRepository;
    private final AttractionRepository attractionRepository;
    private final ShowplaceRepository showplaceRepository;
    private final PlaceRepository placeRepository;

    public PackServiceImpl(PackRepository packRepository, AttractionRepository attractionRepository, ShowplaceRepository showplaceRepository, PlaceRepository placeRepository) {
        this.packRepository = packRepository;
        this.attractionRepository = attractionRepository;
        this.showplaceRepository = showplaceRepository;
        this.placeRepository = placeRepository;
    }


    @Override
    public List<PackDto> getAll() {
        List<Pack> packs = packRepository.findAll();
        return packs.stream().map(pack -> {
            List<AttractionDto> attractionDtoList = attractionRepository.findAllByIdIn(pack.getAttractions()).stream().map(AttractionDto::of).collect(Collectors.toList());
            List<ShowPlaceDto> showPlaceDtos = showplaceRepository.findAllByIdIn(pack.getShowplaces()).stream().map(ShowPlaceDto::of).collect(Collectors.toList());
            List<PlaceDto> placeList = placeRepository.findAllByIdIn(pack.getPlaces()).stream().map(PlaceDto::of).collect(Collectors.toList());
            return new PackDto(pack.getId(),
                    pack.getUser(),
                    pack.getMessage(),
                    attractionDtoList,
                    showPlaceDtos,
                    placeList,
                    pack.getCost());
        }).collect(Collectors.toList());


    }

    @Override
    public String createPack(CreatePackRequestDto createPackRequestDto) {
        return packRepository.save(new Pack(
                createPackRequestDto.getUserId(),
                createPackRequestDto.getMessage(),
                createPackRequestDto.getAttractions(),
                createPackRequestDto.getShowPlaces(),
                createPackRequestDto.getPlaces(),
                null
        )).getId();
    }
}
