package terekhov.digital.breakthrough.backend.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import terekhov.digital.breakthrough.backend.dto.AttractionDto;
import terekhov.digital.breakthrough.backend.repository.AttractionRepository;
import terekhov.digital.breakthrough.backend.service.AttractionService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AttractionServiceImpl implements AttractionService {
    private static final Logger log = LoggerFactory.getLogger(AttractionService.class);
    private final AttractionRepository attractionRepository;

    public AttractionServiceImpl(AttractionRepository attractionRepository) {
        this.attractionRepository = attractionRepository;
    }

    @Override
    public List<AttractionDto> getAll() {
        return attractionRepository.findAll().stream().map(AttractionDto::of).collect(Collectors.toList());
    }
}
