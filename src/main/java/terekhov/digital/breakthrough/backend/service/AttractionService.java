package terekhov.digital.breakthrough.backend.service;

import terekhov.digital.breakthrough.backend.dto.AttractionDto;

import java.util.List;

public interface AttractionService {

    List<AttractionDto> getAll();
}
