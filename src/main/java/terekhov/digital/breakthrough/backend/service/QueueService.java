package terekhov.digital.breakthrough.backend.service;


import terekhov.digital.breakthrough.backend.dto.QueueDto;

public interface QueueService {


    QueueDto joinQueue(String queueId);

    QueueDto getStatus(String innerQueueId);

    String leaveQueue(String innerQueueId);
}
