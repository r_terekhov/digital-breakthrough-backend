package terekhov.digital.breakthrough.backend.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import terekhov.digital.breakthrough.backend.domain.Queue;
import terekhov.digital.breakthrough.backend.domain.QueueInner;
import terekhov.digital.breakthrough.backend.dto.QueueDto;
import terekhov.digital.breakthrough.backend.repository.QueueInnerRepository;
import terekhov.digital.breakthrough.backend.repository.QueueRepository;
import terekhov.digital.breakthrough.backend.service.QueueService;
import terekhov.digital.breakthrough.backend.utils.Utils;

import java.time.Instant;
import java.util.List;

@Service
public class QueueServiceImpl implements QueueService {
    private static final Logger log = LoggerFactory.getLogger(QueueService.class);
    private final QueueRepository queueRepository;
    private final QueueInnerRepository queueInnerRepository;


    public QueueServiceImpl(QueueRepository queueRepository, QueueInnerRepository queueInnerRepository) {
        this.queueRepository = queueRepository;
        this.queueInnerRepository = queueInnerRepository;
    }


    @Override
    public QueueDto joinQueue(String queueId) {
        Queue queue = queueRepository.findById(queueId).get();
        List<QueueInner> queueInners = queueInnerRepository.findAllByCreatedBefore(Instant.now().getEpochSecond());

        QueueInner queueInner = queueInnerRepository.save(new QueueInner(queueId, Utils.randomString()));

        return new QueueDto(queueInner.getId(), queueInner.getCode(), queue.getVisitable(),
                queueInners.size() * queue.getTimeForOnePerson(), queueInners.size());

    }

    @Override
    public QueueDto getStatus(String innerQueueId) {

        QueueInner queueInner = queueInnerRepository.findById(innerQueueId).get();
        Queue queue = queueRepository.findById(queueInner.getQueueId()).get();
        List<QueueInner> queueInners = queueInnerRepository.findAllByCreatedBefore(queueInner.getCreated());

        return new QueueDto(queueInner.getId(), queueInner.getCode(), queue.getVisitable(),
                queueInners.size() * queue.getTimeForOnePerson(), queueInners.size());
    }

    @Override
    public String leaveQueue(String innerQueueId) {
        queueInnerRepository.deleteById(innerQueueId);
        return "OK";
    }
}
