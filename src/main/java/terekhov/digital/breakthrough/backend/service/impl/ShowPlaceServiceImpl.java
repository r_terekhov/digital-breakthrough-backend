package terekhov.digital.breakthrough.backend.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import terekhov.digital.breakthrough.backend.dto.ShowPlaceDto;
import terekhov.digital.breakthrough.backend.repository.ShowplaceRepository;
import terekhov.digital.breakthrough.backend.service.ShowPlaceService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ShowPlaceServiceImpl implements ShowPlaceService {
    private static final Logger log = LoggerFactory.getLogger(ShowPlaceService.class);
    private final ShowplaceRepository showplaceRepository;

    public ShowPlaceServiceImpl(ShowplaceRepository showplaceRepository) {
        this.showplaceRepository = showplaceRepository;
    }

    @Override
    public List<ShowPlaceDto> getAll() {
        return showplaceRepository.findAll().stream().map(ShowPlaceDto::of).collect(Collectors.toList());
    }
}
