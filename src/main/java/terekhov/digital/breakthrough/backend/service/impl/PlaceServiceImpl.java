package terekhov.digital.breakthrough.backend.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import terekhov.digital.breakthrough.backend.dto.PlaceDto;
import terekhov.digital.breakthrough.backend.repository.PlaceRepository;
import terekhov.digital.breakthrough.backend.service.PlaceService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlaceServiceImpl implements PlaceService {
    private static final Logger log = LoggerFactory.getLogger(PlaceService.class);
    private final PlaceRepository placeRepository;

    public PlaceServiceImpl(PlaceRepository placeRepository) {
        this.placeRepository = placeRepository;
    }

    @Override
    public List<PlaceDto> getAllPlaces() {
        return placeRepository.findAll().stream().map(PlaceDto::of).collect(Collectors.toList());
    }
}
