package terekhov.digital.breakthrough.backend.service;

import terekhov.digital.breakthrough.backend.dto.UserDto;

public interface UserService {

    UserDto signUp();
}
