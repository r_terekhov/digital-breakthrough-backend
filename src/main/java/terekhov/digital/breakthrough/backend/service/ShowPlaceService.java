package terekhov.digital.breakthrough.backend.service;

import terekhov.digital.breakthrough.backend.dto.ShowPlaceDto;

import java.util.List;

public interface ShowPlaceService {

    List<ShowPlaceDto> getAll();
}
