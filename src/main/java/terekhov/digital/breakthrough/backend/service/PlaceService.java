package terekhov.digital.breakthrough.backend.service;


import terekhov.digital.breakthrough.backend.dto.PlaceDto;

import java.util.List;

public interface PlaceService {

    List<PlaceDto> getAllPlaces();

}
