package terekhov.digital.breakthrough.backend.service;

import terekhov.digital.breakthrough.backend.dto.CreatePackRequestDto;
import terekhov.digital.breakthrough.backend.dto.PackDto;

import java.util.List;

public interface PackService {

    List<PackDto> getAll();

    String createPack(CreatePackRequestDto createPackRequestDto);


}
