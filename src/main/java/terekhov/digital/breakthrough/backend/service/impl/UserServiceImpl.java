package terekhov.digital.breakthrough.backend.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import terekhov.digital.breakthrough.backend.domain.User;
import terekhov.digital.breakthrough.backend.dto.UserDto;
import terekhov.digital.breakthrough.backend.repository.UserRepository;
import terekhov.digital.breakthrough.backend.service.UserService;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger log = LoggerFactory.getLogger(UserService.class);
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDto signUp() {
        return UserDto.of(userRepository.save(new User()));
    }
}
