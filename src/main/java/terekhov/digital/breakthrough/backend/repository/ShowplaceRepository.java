package terekhov.digital.breakthrough.backend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import terekhov.digital.breakthrough.backend.domain.ShowPlace;

import java.util.List;

@Repository
public interface ShowplaceRepository extends MongoRepository<ShowPlace, String> {


    List<ShowPlace> findAllByIdIn(List<String> ids);
}
