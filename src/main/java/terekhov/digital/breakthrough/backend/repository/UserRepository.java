package terekhov.digital.breakthrough.backend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import terekhov.digital.breakthrough.backend.domain.User;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
}
