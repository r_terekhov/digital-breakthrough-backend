package terekhov.digital.breakthrough.backend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import terekhov.digital.breakthrough.backend.domain.Attraction;

import java.util.List;

@Repository
public interface AttractionRepository extends MongoRepository<Attraction, String> {

    List<Attraction> findAllByIdIn(List<String> ids);
}
