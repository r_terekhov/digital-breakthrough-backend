/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package terekhov.digital.breakthrough.backend.repository;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import terekhov.digital.breakthrough.backend.domain.Queue;

@Repository
public interface QueueRepository extends MongoRepository<Queue, String> {

}
