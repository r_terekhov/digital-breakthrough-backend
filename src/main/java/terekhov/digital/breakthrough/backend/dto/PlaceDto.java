package terekhov.digital.breakthrough.backend.dto;

import lombok.Value;
import terekhov.digital.breakthrough.backend.domain.Place;
import terekhov.digital.breakthrough.backend.utils.Utils;

import java.util.List;


@Value
public class PlaceDto {

    String id;

    String name;

    String urlPhoto;

    String description;

    double lat;

    double lng;

    int currentFullness;

    int maxFullness;

    String workingHours;

    String queueId;


    public static PlaceDto of(Place place) {
        List<Double> latLng = Utils.convertLocationToLatLng(place.getLocation());
        return new PlaceDto(place.getId(),
                place.getName(),
                place.getUrlPhoto(),
                place.getDescription(),
                latLng.get(0),
                latLng.get(1),
                place.getCurrentFullness(),
                place.getMaxFullness(),
                place.getWorkingHours(),
                place.getQueueId());
    }

}
