package terekhov.digital.breakthrough.backend.dto;

import lombok.Value;
import terekhov.digital.breakthrough.backend.domain.User;

@Value
public class UserDto {
    String id;


    public static UserDto of(User user) {
        return new UserDto(user.getId());
    }
}
