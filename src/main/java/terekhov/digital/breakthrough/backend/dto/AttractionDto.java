package terekhov.digital.breakthrough.backend.dto;

import lombok.Value;
import terekhov.digital.breakthrough.backend.domain.Attraction;
import terekhov.digital.breakthrough.backend.utils.Utils;

import java.util.List;

@Value
public class AttractionDto {
    String id;

    String name;

    String urlPhoto;

    String description;

    double lat;

    double lng;

    int currentFullness;

    int maxFullness;

    String workingHours;

    String queueId;


    public static AttractionDto of(Attraction attraction) {
        List<Double> latLng = Utils.convertLocationToLatLng(attraction.getLocation());
        return new AttractionDto(attraction.getId(),
                attraction.getName(),
                attraction.getUrlPhoto(),
                attraction.getDescription(),
                latLng.get(0),
                latLng.get(1),
                attraction.getCurrentFullness(),
                attraction.getMaxFullness(),
                attraction.getWorkingHours(),
                attraction.getQueueId());
    }
}
