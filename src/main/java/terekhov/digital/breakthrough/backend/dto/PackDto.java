package terekhov.digital.breakthrough.backend.dto;

import lombok.Value;
import terekhov.digital.breakthrough.backend.domain.Pack;

import java.util.List;

@Value
public class PackDto {

    String id;

    String user;

    String message;

    List<AttractionDto> attractions;

    List<ShowPlaceDto> showplaces;

    List<PlaceDto> places;

    int cost;


    public static PackDto of(Pack pack,
                             List<AttractionDto> attractions,
                             List<ShowPlaceDto> showPlaces,
                             List<PlaceDto> places) {
        return new PackDto(pack.getId(),
                pack.getUser(),
                pack.getMessage(),
                attractions,
                showPlaces,
                places,
                pack.getCost());
    }

}
