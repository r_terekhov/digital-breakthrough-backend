package terekhov.digital.breakthrough.backend.dto;

import lombok.Value;

import java.util.List;

@Value
public class CreatePackRequestDto {
    String userId;

    String message;

    List<String> attractions;

    List<String> showPlaces;

    List<String> places;
}
