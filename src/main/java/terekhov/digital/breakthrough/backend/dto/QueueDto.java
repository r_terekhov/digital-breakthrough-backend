package terekhov.digital.breakthrough.backend.dto;

import lombok.Value;

@Value
public class QueueDto {

    String queueInnerId;

    String code;

    String visitable;

    int waitingTime;

    int beforeYou;

}
