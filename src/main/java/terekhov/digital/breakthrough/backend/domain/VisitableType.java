package terekhov.digital.breakthrough.backend.domain;

import lombok.Getter;

public enum VisitableType {
    ATTRACTION("ATTRACTION"),
    SHOWPLACE("SHOWPLACE"),
    PLACE("PLACE");

    @Getter
    private String type;


    VisitableType(String type) {
        this.type = type;
    }
}
