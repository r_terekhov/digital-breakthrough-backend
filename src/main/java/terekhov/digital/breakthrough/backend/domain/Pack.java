package terekhov.digital.breakthrough.backend.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Pack extends Document {

    private String user;

    private String message;

    private List<String> attractions;

    private List<String> showplaces;

    private List<String> places;

    private Integer cost;
}
