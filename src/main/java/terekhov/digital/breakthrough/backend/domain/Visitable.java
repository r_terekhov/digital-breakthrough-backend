package terekhov.digital.breakthrough.backend.domain;

public interface Visitable {

    String getType();
}
