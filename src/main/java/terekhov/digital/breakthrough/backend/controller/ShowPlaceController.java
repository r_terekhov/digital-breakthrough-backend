package terekhov.digital.breakthrough.backend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import terekhov.digital.breakthrough.backend.dto.ShowPlaceDto;
import terekhov.digital.breakthrough.backend.model.BaseResponse;
import terekhov.digital.breakthrough.backend.service.ShowPlaceService;

import java.util.List;

@RestController
@Validated
public class ShowPlaceController {
    private static final Logger log = LoggerFactory.getLogger(ShowPlaceController.class);
    private final ShowPlaceService showPlaceService;

    public ShowPlaceController(ShowPlaceService showPlaceService) {
        this.showPlaceService = showPlaceService;
    }


    @GetMapping("/showplace/getAll")
    public ResponseEntity<BaseResponse<List<ShowPlaceDto>>> getAll() {
        return new ResponseEntity<>(BaseResponse.response(showPlaceService.getAll()), HttpStatus.OK);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ResponseEntity<BaseResponse> onInvalidRequest(Exception ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
