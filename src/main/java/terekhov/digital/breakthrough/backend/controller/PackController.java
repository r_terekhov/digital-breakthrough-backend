package terekhov.digital.breakthrough.backend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import terekhov.digital.breakthrough.backend.dto.CreatePackRequestDto;
import terekhov.digital.breakthrough.backend.dto.PackDto;
import terekhov.digital.breakthrough.backend.model.BaseResponse;
import terekhov.digital.breakthrough.backend.service.PackService;

import java.util.List;


@RestController
public class PackController {
    private static final Logger log = LoggerFactory.getLogger(PackController.class);
    private final PackService packService;

    public PackController(PackService packService) {
        this.packService = packService;
    }


    @PostMapping("/pack/create")
    public ResponseEntity<BaseResponse<String>> join(
            @RequestBody CreatePackRequestDto createPackRequestDto) {
        return new ResponseEntity<>(BaseResponse.response(packService.createPack(createPackRequestDto)), HttpStatus.OK);
    }

    @GetMapping("/pack/getAll")
    public ResponseEntity<BaseResponse<List<PackDto>>> getAll() {
        return new ResponseEntity<>(BaseResponse.response(packService.getAll()), HttpStatus.OK);
    }


    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ResponseEntity<BaseResponse> onInvalidRequest(Exception ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
