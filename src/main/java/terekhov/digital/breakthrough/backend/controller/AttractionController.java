package terekhov.digital.breakthrough.backend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import terekhov.digital.breakthrough.backend.dto.AttractionDto;
import terekhov.digital.breakthrough.backend.model.BaseResponse;
import terekhov.digital.breakthrough.backend.service.AttractionService;

import java.util.List;

@RestController
@Validated
public class AttractionController {
    private static final Logger log = LoggerFactory.getLogger(AttractionController.class);
    private final AttractionService attractionService;

    public AttractionController(AttractionService attractionService) {
        this.attractionService = attractionService;
    }


    @GetMapping("/attraction/getAll")
    public ResponseEntity<BaseResponse<List<AttractionDto>>> getAll() {
        return new ResponseEntity<>(BaseResponse.response(attractionService.getAll()), HttpStatus.OK);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ResponseEntity<BaseResponse> onInvalidRequest(Exception ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
